#pragma once
#include <Streaming.h>

class log_pwm {
    /*
      Build a table for log curve values, for the number of bits.
      The exponential curve is **1.02, based on empirical "2% is visibly smooth".
      I call it "log" because I developed the idea based on decay rate, rather than growth.

      You can change this object to a different bit size, whenever you want.

      this_object->max_value is the maximum value that we can convert to hardware pwm.
      this_object(somevalue) returns the hardware pwm value
        for an integer `somevalue`, has to be 0 .. this_object->max_value
        for a float, `somevalue` is 0.0 .. 1.0
          (the "resolution" is 1 / this_object->max_value )
          The float version is UTTERLY untested.
          
      This is very unrefined. e.g. you must create a pointer at the global level,
      because I print to Serial at constructor time.
      And, I statically allocate the maximum possible table entries,
      and that is actually too large.
      And, the logic for the max value is not quite right, and repeats for some bit values.

    */

  public:
    // static constexpr float DECAY = 0.03; // The empirical "smooth" value
    // static constexpr int MAX_H_VALUES = 376; // log(16bits, base 1.03) + 1. so max size
    static constexpr float DECAY = 0.02; // The empirical "smooth" value
    static constexpr int MAX_H_VALUES = 561; // log(16bits, base 1.02) + 1. so max size
    static constexpr int MAX_BITS = 16;
    static constexpr int MIN_BITS = 8;
    int hardware_values[MAX_H_VALUES]; //
    int max_value; // in "curve" world, the max. so 0..max_value, inclusive

    log_pwm(int bits = 8) {
      build_table(bits);
    }

    void build_table(int bits) {
      // actual values, skipping repeats
      hardware_values[0] = 0;
      int i = 1;
      for (float v = 1.0; int(v) < pow(2, bits); v = v * (1 + DECAY)) {
        if (int(v) != hardware_values[i - 1]) {
          hardware_values[i] = v;
          Serial << F("[") << i << F("] ") << int(v) << endl;
          i++;
          if (max_value > MAX_H_VALUES) {
            Serial << F("FATAL: Calculated table size ") << max_value
                   << F(" bigger than allowed ") << MAX_H_VALUES
                   << F(" for bits ") << bits
                   << F(" @ ") << __FILE__ << F(" line ") << __LINE__
                   << endl;
            while (1) delay(10);
          }
        }
      }
      max_value = i;
      hardware_values[i] = pow(2, bits) - 1;
      Serial << F("  max [") << max_value << F("] ") << hardware_values[max_value] << endl;
    }

    int operator()(int curve_value) {
      // give the hardware pwm value
      if (curve_value > max_value || curve_value < 0) {
        Serial << F("Fatal, log_pwm value must be 0..") << max_value << endl;
        return 0;
      }
      else {
        return hardware_values[ curve_value ];
      }
    }
    int operator()(float curve_value) {
      // give the hardware pwm value
      if (curve_value > 1.0 || curve_value < 0.) {
        Serial << F("Fatal, log_pwm value must be 0.0 .. 1.0") << endl;
        return 0;
      }
      else {
        return hardware_values[ (curve_value * this->max_value) ];
      }
    }

};
