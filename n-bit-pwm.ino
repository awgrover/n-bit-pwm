/*
  n-bit pwm test
  i.e. does `analogWriteResolution()` work?
  
  Put an LED on pin 0 (with current limiting resistor, of course!).
  Change that, and the `pwmpin`, for different boards.

  Runs a fade on the pwm values at the low end,
  so you can see "stepping".
  You can ask for a different pwm-bit count, i.e. 16 gives 65535 possible pwm values.
  You can do a fade on the "high" end.
  log.pwm.h is an unrefined "logarithmic" correction curve:
    so a values look like linear changes to the eye.

  Run this code with the serial console open.
  To change modes/settings, type '?' and send.
  Then choose an option.
*/

#include <Streaming.h>
#include <id.h>
#include <every.h>

// Turn off the annoyingly bright built in dotstar
#ifdef PIN_DOTSTAR_DATA
#include <Adafruit_DotStar.h>
Adafruit_DotStar dotstar(DOTSTAR_NUM, PIN_DOTSTAR_DATA, PIN_DOTSTAR_CLK, DOTSTAR_BRG);
#endif
#include "log_pwm.h"

int pwmpin = 0;
log_pwm *pwm_by_log; // my "logarithmic" curve function

void setup() {
#ifdef PIN_DOTSTAR_DATA
  dotstar.begin(); dotstar.setPixelColor(0, 0); dotstar.show();
#endif
  static Every serial_wait(3 * 1000); // how long before continuing
  static Every fast_flash(50);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pwmpin, OUTPUT); digitalWrite(pwmpin, HIGH);
  Serial.begin(115200);
  // always wait, assume serial will work after serial_wait time
  while (! serial_wait() && ! Serial) {
    if (fast_flash()) digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
    delay(10); // the delay allows upload to interrupt
  }

  id();
#ifdef PIN_DOTSTAR_DATA
  Serial << F("has dotstar") << endl;
#endif

  pwm_by_log = new log_pwm(8);

}

void loop() {
  static char command = 0xFB; // default is show prompt
  static Every change_pwm(50);
  int new_bits = 0;
  static int cur_bits = 0;
  static int steps = 0;
  static char mode = 'n';
  static char which_end = 'b';

  switch (command) {
    // set command to '?' to display menu w/prompt
    // set command to -1 to prompt
    // set command to -2 to just get input

    // Will make a display block if a case looks like:
    // case 'a-z0-9A-Z': \s // display text

    case 0xFB: // start
      new_bits = 8;
      command = 0xFC;
      break;

    case '8': // 8 bits
      new_bits = 8;
      command = 0xFD;
      break;
    case 'a': // 10 bits
    case 'c': // 12 bits
    case 'e': // 14 bits
      new_bits = command - 'a' + 10;
      command = 0xFD;
      break;
    case 'f': // 16 bits
      new_bits = 16;
      command = 0xFD;
      break;

    case 'l' : // use log curve fade
      Serial << F("Log mode") << endl;
      mode = 'l';
      command = 0xFD;
      break;
    case 'n' : // use linear curve fade
      Serial << F("Normal (linear) mode") << endl;
      mode = 'n';
      command = 0xFD;
      break;

    case 't' : // show high end of values
      Serial << F("Top end") << endl;
      which_end = 't';
      command = 0xFD;
      break;
    case 'b' : // show low end of values
      Serial << F("Bottom end") << endl;
      which_end = 'b';
      command = 0xFD;
      break;
    case '?': // show menu & prompt
    case 0xFC : // just menu
      Serial << F("Bits ") << cur_bits << F(" steps ") << steps << endl;
      // menu made by: make (menu.mk):
      Serial.println(F("8  8 bits"));
      Serial.println(F("a  10 bits"));
      Serial.println(F("c  12 bits"));
      Serial.println(F("e  14 bits"));
      Serial.println(F("f  16 bits"));
      Serial.println(F("l  use log curve fade"));
      Serial.println(F("n  use linear curve fade"));
      Serial.println(F("t  show high end of values"));
      Serial.println(F("b  show low end of values"));
      Serial.println(F("?  show menu & prompt"));
      // end menu
      if (command == 0xFC) {
        command = 0xFD;
        break;
      }
    case 0xFF : // show prompt, get input
      Serial.print(F("Choose (? for help): "));
    // fallthrough

    case 0xFE : // just get input
      while (Serial.available() <= 0) {}
      command = Serial.read();
      Serial.println(command);
      return;
      break;

    case 0xFD : // run
      if (Serial.available() > 0) {
        command = Serial.read();
        Serial.println(command);
        return; // force case again
      }
      break;

    default : // show help if not understood
      delay(10); while (Serial.available() > 0) {
        Serial.read();  // empty buffer
        delay(10);
      }
      command = '?';
      break;
  }

  if (new_bits != 0) {
    cur_bits = new_bits;
    new_bits = 0;
    steps = (cur_bits - 7) * 10;  // 10 for 8, 100 for 16?
    pwm_by_log->build_table(cur_bits);
    Serial << F("Bits ") << cur_bits << F(" steps ") << steps << endl;
    analogWriteResolution(cur_bits);
  }

  if (mode == 'n') {
    if (which_end == 'b') {
      for (int dir = 0; dir <= 1; dir++) {
        if (Serial.available() > 0) break;
        for (int v = 1; v <= steps; v++) {
          if (Serial.available() > 0) break;
          //Serial << v << endl;
          analogWrite(pwmpin, dir ? (dir * steps) - v : v);
          delay( 100 );
        }
      }
    }
    else if (which_end == 't') {
      for (int cycles = 0; cycles < 4; cycles++) {
        int range = (pow(2, cur_bits) - 1) / 10; // top 10%
        for (int dir = 0; dir <= 1; dir++) {
          if (Serial.available() > 0) break;
          for (int v = 1; v <= range; v++) {
            if (Serial.available() > 0) break;
            int cv = (pow(2, cur_bits) - 1) - (dir ? (dir * range) - v : v);
            Serial << cv << F(" ") << cv << endl;
            analogWrite(pwmpin, cv);
            delay( 10 );
          }
        }
      }
    }
  }


  else if (mode == 'l') {
    if (which_end == 'b') {
      int range = pwm_by_log->max_value / 3; // bottom 10%
      for (int dir = 0; dir <= 1; dir++) {
        if (Serial.available() > 0) break;
        for (int v = 1; v <= range; v++) {
          if (Serial.available() > 0) break;
          int cv = dir ? (dir * range) - v : v;
          Serial << cv << F(" ") << (*pwm_by_log)(cv) << endl;
          analogWrite(pwmpin, (*pwm_by_log)(cv));
          delay( 100 );
        }
      }
    }

    else if (which_end == 't') {
      for (int cycles = 0; cycles < 4; cycles++) {
        int range = pwm_by_log->max_value / 10; // top 10%
        for (int dir = 0; dir <= 1; dir++) {
          if (Serial.available() > 0) break;
          for (int v = 1; v <= range; v++) {
            if (Serial.available() > 0) break;
            int cv = pwm_by_log->max_value - (dir ? (dir * range) - v : v);
            Serial << cv << F(" ") << (*pwm_by_log)(cv) << endl;
            analogWrite(pwmpin, (*pwm_by_log)(cv));
            delay( 10 );
          }
        }
      }
    }
  }
}
