# n-bit-pwm test

I wanted to see if `analogWriteResolution()` worked on various boards (SAMD21, SAMD51, maybe RP2040).

See the `n-bit-pwm.ino` for instructions on trying this out: this does a fade so you can see if it looks smooth.

The problem is that an artist was doing some specific color changes using LEDs, and at smaller PWM values (staring around 30),
a change in value of `1` was a visible "step", rather than appearing like a smooth fade (with 8-bit PWM).

We used an external 16 bit pwm chip (TLC59711), which worked great. But, I noticed a comment somewhere that the SAMDxx
chips can do 16 bit PWM. And there are several boards with the 12 PWM pins we need (Adafruit's itsybitsy M0 and M4, for example).
Yet, I could find no library to access the 16 bit setting.

I couldn't find a library because you shouldn't need a library. The `analogWriteResolution()` can set the number of PWM bits. It is only documented for the Arduino Zero, but many board manufacturers support it, they just don't tell you about it. E.g. Adafruit supports it (at least for some boards). I ran across it while looking at the code that defines an Adafruit board.

And, it worked on the first board I tried: an ItsyBitsy M0.
